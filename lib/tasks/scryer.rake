require 'json'
require 'scryer/constants'

namespace :scryer do
  desc "Imports all data files"
  task :import => [:import_characters, :import_categories, :import_languages, :import_fandoms] do

  end

  desc "Imports character names from DLP API"
  task import_characters: :environment do
    # TODO
  end

  desc "Imports fandoms"
  task import_fandoms: :environment do
    FandomCategory.delete_all
    Fandom.delete_all

    JSON.parse(File.open('data/fandom_category.json').read).each do |c|
      fc = FandomCategory.new(c)
      fc.save!
    end

    JSON.parse(File.open('data/fandoms.json').read).each do |f|
      fd = Fandom.new(f)
      fd.save!
    end

    # 2016-04-13 crawled fandoms
    Fandom.where(:id =>[
        224,68,1833,7,8,1402,2489,1758,382,4254,13,721,2002,9748,2927,1342,9786,8324,
                        10833,21,7190,2622,118,1960,1536,1968,17,1270,7763,1303,2686,12,104,490,1703,1977,1434
    ]).update_all(:is_indexed => true)
  end

  desc "Imports categories from JSON fixture"
  task import_categories: :environment do
    JSON.parse(File.open('data/categories.json').read).each do |c|
      c = Category.new c
      c.save!
    end
  end

  desc "Imports languages from JSON fixture"
  task import_languages: :environment do
    JSON.parse(File.open('data/languages.json').read).each do |l|
      c = Language.new :name => l
      c.save!
    end
  end

  desc "Imports character genders from JSON fixture"
  task import_character_genders: :environment do
    JSON.parse(File.open('data/character_gender.json').read).each do |g|
      c = Character.find_by_id(g['id'])

      if c
        c.isfemale = g['isfemale']
        c.save!
      end
    end
  end

  desc "Import system tags from content tagger"
  task import_system_tags: :environment do
    Scryer::Constants::TAGS.each do |tag|
      t = Tag.find_or_create_by(name: tag, system: true)
    end
  end

end
