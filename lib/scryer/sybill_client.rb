require 'faraday'

module Scryer
  class SybillClient
    def initialize(api_base='http://127.0.0.1:1777')
      @conn = Faraday.new(:url => api_base) do |faraday|
        faraday.request  :json
        faraday.response :rashify
        faraday.response :json
        faraday.response :logger, ::Logger.new(STDOUT), bodies: true
        faraday.use :instrumentation
        faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
        faraday.headers['User-Agent'] = 'Scryer'
      end
    end

    def recommend(user_id, allowed_fandoms = nil, allow_seen_items = false, num_neighbors = 50, min_users_per_recommendation = 1)
      body = {
          allow_seen_items: allow_seen_items,
          user_id: user_id,
          model_config: {
              num_neighbors: num_neighbors,
              min_users_per_recommendation: min_users_per_recommendation
          }
      }

      if allowed_fandoms
        body[:allowed_fandoms] = allowed_fandoms
      end

      resp = @conn.post do |req|
        req.url "/recommend/stories"
        req.body = body
      end

      if resp.status == 404
        []
      else
        resp.body
      end
    end
  end
end
