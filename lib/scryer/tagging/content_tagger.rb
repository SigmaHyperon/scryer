module Scryer
  module Tagging
    module ContentTagger
      def self.tag(story, relationships = nil)
        tags = []

        relationships ||= story.relationships

        if story.summary =~ /multi\-pairings|(?:\/|x)\s?multi/i || relationships.any? { |r| r.size == 3 }
          if story.summary.count('/') >= 3
            tags << :harem
          else
            tags << :multi
          end
        end

        if story.summary =~ /harem/i || relationships.any? { |r| r.size > 3 }
          tags << :harem
        end

        if story.summary =~ /(^|\s|\*|\(|\/|pre-)slash|mpreg|yaoi|m\/m|\bhp\/?dm|\bhphp\/?ss|\bhptr|\bdrarry/i && story.summary !~ /no(?:t)?(?: a)? slash/i
          tags << :slash
        end

        relationships.each { |group|
          genders = group.inject(Hash.new(0)) { |h, v| h[v.isfemale] += 1; h }

          tags << :slash if genders[false] > 1
          tags << :femslash if genders[true] > 1
        }

        if story.summary =~ /fem(me)?(\s|!)?slash/i
          tags << :femslash
        end

        tags << :gender_bender if story.summary =~ /fem!|fem(?:\s)?harry/i

        tags.uniq
      end

      private

    end
  end
end
