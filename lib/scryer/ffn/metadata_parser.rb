require 'virtus'
require 'pp'

module Scryer
  module FFN
    module MetadataParser
      @translations = Character.where("name LIKE '%,%' OR name LIKE '% - %'")
                          .map{|c| [c.name, c.name.gsub('-', '_').gsub(',', ';')]}
                          .to_h
                          .freeze
      @categories = Set.new(Category.all_cached.map(&:name)).freeze
      @languages = Language.all_cached.to_a.index_by { |l| l.name }.freeze

      def self.parse_info_string(str, fandoms)

        # Translations to make
        # Calendar, J.
        # Jenny - Doctor's Daughter

        # Split on ' - '

        translated_str = str
        @translations.each do |k, v|
          translated_str.gsub!(k, v)
        end

        parts = str.split(' - ')

        m = Metadata.new


        parts = parts.delete_if { |part|
          case
            when part.match(/Chapters: /)
              m.chapters = self.parse_num(part)
              true
            when part.match(/Favs: /)
              m.favs = self.parse_num(part)
              true
            when part.match(/Follows: /)
              m.follows = self.parse_num(part)
              true
            when part.match(/Rated: /)
              m.rated = part.split(': ')[1].gsub('+', 'PLUS').gsub('Fiction ', '').strip
              true
            when part.match(/Reviews: /)
              m.reviews = self.parse_num(part)
              true
            when part.match(/Words: /)
              m.words = self.parse_num(part)
              true
            when part.match(/Published:/)
              true
            when part.match(/Updated:/)
              true
            when part.match(/id:/)
              true
            when part == 'In-Progress'
              m.status = part
              true
            when part == 'Complete'
              m.status = part
              true
            when part == 'Status: Complete'
              m.status = part.gsub('Status: ', '').strip
              true
            when @languages.has_key?(part)
              m.language = part
              true
            when m.categories.empty? && m.words == 0 && @categories.any? { |c| part.include? c }
              p = part.gsub('Hurt/Comfort', 'HC')
              category_names = p.split('/')
              category_names = category_names.map { |c| c.gsub('HC', 'Hurt/Comfort') }
              category_list = Category.where(:name => category_names).to_a
              categories_by_name = category_list.group_by(&:name)
              m.categories = category_names.map { |category| categories_by_name[category][0].attributes }
              true
            else
              character_names = part.split(/[\[\],]/).map(&:strip).reject(&:empty?)

              # Untranslate names
              character_names = character_names.map do |name|
                @translations.each do |k,v|
                  name.gsub!(v, k)
                end

                name
              end

              # Zhang Fei is a FFN glitch, character id 65535. Neato.
              character_names.reject! { |c| c == 'Zhang Fei' }

              characters = lookup_characters(character_names, fandoms)
              characters_by_name = characters.group_by(&:name)
              characters = character_names.map do |character|
                clist = characters_by_name[character]

                if clist.nil?
                  raise "Could not find character: #{character}"
                end

                clist[0]
              end

              m.characters = characters.map(&:attributes)

              characters_by_name = characters.group_by(&:name)
              pairings = part.scan(/\[[^\]]+\]/).map do |pair|
                pair.split(/[\[\],]/).map(&:strip).reject(&:empty?)
              end.map do |names|
                {:characters => names.map { |name|
                    @translations.each do |k,v|
                      name.gsub!(v, k)
                    end

                    characters_by_name[name][0].attributes
                  }
                }
              end

              m.relationships = pairings

              true
          end
        }

        # Left with unhandled pieces
        unless parts == []
          puts "Unhandled pieces (#{str}):"
          puts parts
        end

        # Find status


        m
      end

      def self.parse_num(part)
        part.split(': ')[1].gsub(',', '').gsub('"', '').strip
      end

      private

      def self.lookup_characters(character_names, fandoms)
        Character.where(:name => character_names, :fandom_id => fandoms).to_a
      end
    end

    class MetaCategory
      include Virtus.model

      attribute :id, Integer
      attribute :name, String
    end

    class MetaCharacter
      include Virtus.model

      attribute :id, Integer
      attribute :name, String
    end

    class MetaRelationship
      include Virtus.model

      attribute :characters, Array[MetaCharacter]
    end

    class Metadata
      include Virtus.model

      # status, characters, relationships, rated, wordcount, reviews, favs, follows, language
      attribute :categories, Array[MetaCategory], :default => []
      attribute :chapters, Integer
      attribute :characters, Array[MetaCharacter], :default => []
      attribute :favs, Integer, :default => 0
      attribute :follows, Integer, :default => 0
      attribute :language, String, :default => 'English'
      attribute :status, String, :default => 'In-Progress'
      attribute :tags, Array[String], :default => []
      attribute :rated, String
      attribute :relationships, Array[MetaRelationship], :default => []
      attribute :reviews, Integer, :default => 0
      attribute :words, Integer, :default => 0
    end
  end
end
