Rails.configuration.taglines = [
    'Fanfiction Search Engine',
    'Fanfiction Search Engine',
    'Fanfiction Search Engine',
    'Fanfiction Search Engine',
    'Purveyors of slightly shoddy fiction.',
    'Guilty pleasures abound!',
    'We have what you\'re looking for. Probably.',
    'It\'s been written a thousand times already. Why not do it again?',
    'That\'s original.',
    'Enabler of questionable tastes.',
    'We <strong>always</strong> cross the streams.',
    'Bringing you your own harem of stories.',
    'Not just a smut database.',
    'Quality Fanfiction will always be given to those who ask for it.',
    'Finding your next great adventure.'
].freeze
