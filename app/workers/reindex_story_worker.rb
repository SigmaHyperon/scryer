class ReindexStoryWorker < StoryIndexBase
  include Sidekiq::Worker
  sidekiq_options :backtrace => true

  def perform(story_id, should_index_in_es)
    story_row = Story.find_by(id: story_id)
    index_name = story_row.es_index
    is_crossover = story_row.crossover?


    story = $elasticsearch.get index: 'ffn_index', id: story_id, ignore: 404, type: 'story'
    story_crossover = $elasticsearch.get index: 'ffncrossover_index', id: story_id, ignore: 404, type: 'story'

    if !story['found'] and !story_crossover['found']
      logger.info "[#{story_id}] Cannot find document in ES, skipping reindex."
      return
    end

    if is_crossover and story['found']
      logger.warn "[#{story_id}] Found crossover in normal index, deleting"
      $elasticsearch.delete index: 'ffn_index', id: story_id, ignore: 404, type: 'story'
    elsif !is_crossover and story_crossover['found']
      logger.warn "[#{story_id}] Found normal story in crossover index, deleting"
      $elasticsearch.delete index: 'ffncrossover_index', id: story_id, ignore: 404, type: 'story'
    end


    if story_row.deleted?
      logger.info "[#{story_id}] Story is marked at deleted, removing from index."
      $elasticsearch.delete index: index_name, id: story_id, ignore: 404, type: 'story'
      return
    end

    logger.info "[#{story_id}] Reindexing story."

    if is_crossover
      story = story_crossover
    end

    index(story_id, story['_source'], should_index_in_es)
  end
end
