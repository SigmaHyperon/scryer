class IndexFandomWorker
  include Sidekiq::Worker
  sidekiq_options :backtrace => true

  def perform(fandom_id, body)
    logger.info "[#{fandom_id}] Indexing fandom from Crawler"

    if fandom_id.to_i == 0
      logger.info "[#{fandom_id}] Skipping fandom."
      return
    end

    fandom = Fandom.find_or_initialize_by(id: fandom_id)
    fandom.name = body['name'].strip

    if fandom.name == ""
      raise "Bad fandom: #{body}"
    end

    fandom.save!
  end
end
