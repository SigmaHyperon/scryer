class ReindexDlpScoresWorker
  include Sidekiq::Worker
  sidekiq_options :backtrace => true

  def perform
    Story.joins(:story_to_thread).find_each(&:reindex)
  end
end
