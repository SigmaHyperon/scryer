class IndexCharacterWorker
  include Sidekiq::Worker
  sidekiq_options :backtrace => true

  def perform(character_id, body)
    logger.info "[#{character_id}] Indexing character from Crawler"

    if character_id.to_i == 0
      logger.info "[#{character_id}] Skipping 'All Characters' character."
      return
    end

    character = Character.find_or_initialize_by(id: character_id)
    character.name = body['name']
    character.fandom_id = body['fandom_id'].to_i
    character.save!
  end
end
