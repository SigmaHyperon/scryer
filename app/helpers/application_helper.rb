module ApplicationHelper
  def favicon_name
    if Rails.env.development?
      'favicon_development.ico'
    else
      'favicon.ico'
    end
  end

  def active_link(link)
    'active' if current_page?(link)
  end

  def feature_active?(feature_name)
    if @current_user
      $rollout.active?(feature_name, @current_user)
    else
      false
    end

  end

  def flash_class(level)
    case level
    when :notice then "info"
    when :error then "danger"
    when :alert then "warning"
    end
  end
end
