json.(story, :id, :title)
json.external_url story.url
json.author do
  json.(story.author, :id, :name)
  json.external_url story.author.url
end
json.deleted story.deleted?
json.tags story.tags, :name, :system
json.(story, :published, :updated, :last_seen, :deleted_at)
json.(story, :status, :summary, :language, :rated, :chapters, :favs, :follows, :reviews, :words)
json.characters story.characters, :id, :name, :gender
json.categories story.categories, :name
json.fandoms story.fandoms, :name
json.relationships story.relationships do |relationship|
  json.array! relationship, :id, :name, :gender
end