json.array! @fandoms do |fandom|
  json.fandom_id fandom.id
  json.category_slug fandom.category_slug
  json.name fandom.name
end

