json.array!(@saved_searches) do |saved_search|
  json.extract! saved_search, :id
  json.url saved_search_url(saved_search, format: :json)
end
