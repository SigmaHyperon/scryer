class User::HiddenController < ApplicationController
  before_action :authenticate_user!

  def index
    @authors = @current_user.hidden_authors.includes(:author).order(:created_at)
    @stories = @current_user.hidden_stories.includes(:story, story: [:author]).order(:created_at)
  end

  def hide_author
    @current_user.hidden_authors.find_or_create_by!(author_id: params[:author_id])

    render :status => 200, :json => {status: 200}
  end

  def unhide_author
    @current_user.hidden_authors.where(author_id: params[:author_id]).each(&:destroy)

    render :status => 200, :json => {status: 200}
  end

  def hide_story
    @current_user.hidden_stories.find_or_create_by!(story_id: params[:story_id])

    render :status => 200, :json => {status: 200}
  end

  def unhide_story
    @current_user.hidden_stories.where(story_id: params[:story_id]).each(&:destroy)

    render :status => 200, :json => {status: 200}
  end
end
