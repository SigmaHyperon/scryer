class User::RecommendationsController < ApplicationController
  before_action :authenticate_user!

  def index
    top_fandoms = ActiveRecord::Base.connection.exec_query("
    SELECT rank_filter.* FROM (
      SELECT
        sc.user_id,
        s.fandoms,
        COUNT(*) as clicks,
        rank() OVER (
                  PARTITION BY sc.user_id
                  ORDER BY COUNT(*) DESC, fandoms
              )
      FROM story_clicks sc
      LEFT JOIN stories s
      ON sc.story_id = s.id
      WHERE
        user_id IS NOT NULL
        AND user_id = $1
        AND sc.created_at > (now() - INTERVAL '6 month')
      GROUP BY sc.user_id, s.fandoms
      HAVING COUNT(*) > 1
      ORDER BY COUNT(*) DESC
      ) rank_filter ORDER BY user_id ASC, clicks DESC", 'SQL', [[nil, @current_user.id]])

    fandoms = top_fandoms.map { |f| f["fandoms"].tr('^[0-9],', '').split(',').map { |n| n.to_i } }

    allow_seen_items = ActiveModel::Type::Boolean.new.cast(params[:allow_seen_items] || 'false')
    num_neighbors = (params[:num_neighbors] || '100').to_i
    min_users = (params[:min_users] || '1').to_i

    begin
      recommendations = $sybill.recommend(@current_user.id, fandoms, allow_seen_items, num_neighbors, min_users)
      @recommendation_by_id = recommendations.inject({}) { |h,rec| h[rec['story_id']]= rec; h }


      @story_context = StoryFetchContext.new(current_user, recommendations.map { |recommendation| recommendation['story_id'].to_i })
      @stories = @story_context.stories
    rescue Faraday::ResourceNotFound
      @story_context = nil
      @stories = []
    end
  end
end
