class CrawlController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :check_crawl_key

  respond_to :json

  def generate_assignments
    @assignments = CrawlBookmark
                       .where(bootstrapped: true)
  end

  def indexed_fandoms
    @fandoms = Fandom.joins(:fandom_category).includes(:fandom_category).where(:is_indexed => true)
  end

  def update_checkpoint
    checkpoint = CrawlBookmark.find_or_initialize_by(fandoms: checkpoint_params['fandoms'].sort)

    # Updating a checkpoint means the fandom is bootstrapped and does not need reindexing.
    params = checkpoint_params.merge(
        fandoms: checkpoint_params['fandoms'].sort,
        bootstrapped: true,
        needs_reindex: false)

    # If we reindexed, set the last_reindexed time.
    params.merge!(last_reindexed: DateTime.now) if checkpoint.needs_reindex

    checkpoint.assign_attributes(params)
    if checkpoint.new_record? || checkpoint.changed?
      checkpoint.save!
    else
      checkpoint.touch(:updated_at)
    end
  end

  protected
  def check_crawl_key
    if params[:crawl_key] != Rails.application.secrets.crawl_key
      render status: 401, json: {status: 401}
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def checkpoint_params
    params.require(:crawl).permit(:last_crawled, fandoms: [])
  end
end
