class SavedSearch < ActiveRecord::Base
  @@hash_id_salt = Rails.application.secrets['hash_id_salt']

  hash_id salt: @@hash_id_salt
  belongs_to :user
  has_one :search, :dependent => :destroy

  def hash_id
    return nil if id.nil?

    hashids = Hashids.new(@@hash_id_salt, 3)
    hashids.encode id
  end

  def search_name_with_fandoms
    "#{name} (#{Fandom.name_sentence(search.merged_fandoms)})"
  end
end
