require 'scryer/constants'

class Search < ActiveRecord::Base
  after_initialize :init

  validates :wordcount_lower, :wordcount_upper, :favorites_lower, :favorites_upper, :chapters_lower, :chapters_upper,
            numericality: {only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 2**31-1},
            allow_nil: true
  validates :fandoms, presence: true
  validates :category_required, :length => { :maximum => 2, :message => 'may have at most 2 categories'}
  validates :character_required, :length => { :maximum => 4, :message => 'may have at most 4 characters' }
  validates :status, inclusion: {in: Scryer::Constants::STATUS.values}
  validates :read_filter, inclusion: {in: Scryer::Constants::READ_FILTER.values}, presence: true
  validates :order_by, inclusion: {in: Scryer::Constants::ORDERING.values}, presence: true
  validates :category_optional_exclude, :character_optional_exclude, :inclusion => {:in => [true, false]}
  validates_each :fandoms,
                 :crossovers,
                 :character_required,
                 :character_optional,
                 :category_required,
                 :category_optional do |record, attr, value|
    if !value.nil? && !value.all? { |v| v.is_a?(Integer) }
      record.errors.add(attr, "collection is not an all integers: #{value}")
    end
  end
  validate :validate_sort_by
  validate :validate_ratings
  validate :validate_crossover_options

  belongs_to :user, optional: true
  # has_many :tags_include, :class_name => 'Tag'
  # has_many :tags_exclude, :class_name => 'Tag'

  @@fields = Set.new(%w(fandoms crossovers include_source_fandoms exclude_crossover_fandoms title author summary
                category_required category_optional category_optional_exclude
                character_required character_optional character_optional_exclude
                relationships
                tags_include tags_exclude
                language status rating, read_filter
                wordcount_lower wordcount_upper chapters_lower chapters_upper
                updated_before updated_after published_before published_after
                favorites_lower favorites_upper
                sort_by order_by).map{|f| f.to_sym})

  def init
    # Default values

    self.fandoms                     ||= [224]
    self.crossovers                  ||= []
    self.include_source_fandoms      ||= false
    self.exclude_crossover_fandoms   ||= false
    self.title                       ||= ''
    self.author                      ||= ''
    self.summary                     ||= ''
    self.category_required           ||= []
    self.category_optional           ||= []
    self.category_optional_exclude   ||= false
    self.character_required          ||= []
    self.character_optional          ||= []
    self.character_optional_exclude  ||= false
    self.relationships               ||= []
    self.tags_include_ids            ||= []
    self.tags_exclude_ids            ||= []
    self.language                    ||= 'english'
    self.status                      ||= ''
    self.read_filter                 ||= 'all'
    self.rating                      ||= Scryer::Constants::RATING.values
    self.wordcount_lower             ||= nil
    self.wordcount_upper             ||= nil
    self.favorites_lower             ||= nil
    self.favorites_upper             ||= nil
    self.chapters_lower              ||= nil
    self.chapters_upper              ||= nil
    self.published_before            ||= nil
    self.published_after             ||= nil
    self.updated_before              ||= nil
    self.updated_after               ||= nil
    self.sort_by                     ||= '_score'
    self.order_by                    ||= 'desc'

    # Fix rating
    self.rating.collect! do |rating|
      if rating == 'k+' or rating == 'k '
        'kplus'
      else
        rating
      end
    end

    # Fix relationships
    if self.relationships.is_a? Hash
      self.relationships = self.relationships.values.collect do |ship|
        if ship['characters'] && !ship['characters'].empty?
          ship
        else
          nil
        end
      end.compact
    end
  end

  def execute(user, page, per_page)
    SearchResults.new(
        SearchQuery.new.execute(user, self, (page-1)*per_page, per_page),
        page,
        per_page)
  end

  def allow_stale_results?
    false
  end

  def merged_fandoms
    f = fandoms
    if crossovers
      f += crossovers
    end

    f
  end

  def tags_include
    Tag.where id: tags_include_ids
  end

  def tags_exclude
    Tag.where id: tags_exclude_ids
  end

  def self.from_hash(h)
    # unknown property??
    h.delete('fandom')

    h.each do |k,v|
      if v.is_a?(Array)
        h[k] = v.reject{|v2| v2.nil? || v2 == '' }
      end
    end

    Search.new(h)
  end

  def to_hash
    self.attributes.select { |k,v| @@fields.include?(k.to_sym) && v != nil && v != [nil] }
  end

  def to_reduced_hash
    self.attributes.select { |k,v| @@fields.include?(k.to_sym) && v != nil && v != '' && v != [nil] && v != [] && v }
  end

private
  def validate_ratings
    diff = rating-Scryer::Constants::RATING.values
    unless diff.empty?
      errors.add(:rating, "contains unsupported values, found: #{diff}, allowed: #{Scryer::Constants::RATING.values}")
    end
  end
  def validate_sort_by
    unless Scryer::Constants::SORTING.values.include?(sort_by)
      errors.add(:sort_by, "contains unsupported values, found: #{sort_by}, allowed: #{Scryer::Constants::SORTING.values}")
    end
  end
  def validate_crossover_options
    if crossovers.include? -1 and exclude_crossover_fandoms
      errors.add(:crossovers, "cannot exclude all crossovers")
    end
  end
end

class SearchResults
  attr_reader :search, :results, :hits, :pagination

  def initialize(search_results, page, per_page)
    @search = search_results
    @page = page
    @per_page = per_page
    @hits = @search['hits']


    story_ids = @search['results'].map { |result| result['story_id'] }
    stories_by_id = Story.where(:id => story_ids)
                   .includes([:author, :story_to_thread])
                   .joins([:author])
                   .index_by(&:id)
    @results = story_ids
                   .map do |id|
      story = stories_by_id[id]
      if story.nil?
        Rails.logger.warn "search: Couldn't find Story(#{id})"
      end
      story
    end.compact


    @pagination = Kaminari.paginate_array(@results, total_count: @hits).page(@page).per(@per_page)
  end
end
