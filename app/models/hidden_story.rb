class HiddenStory < ApplicationRecord
  after_save :reindex_story
  after_destroy :reindex_story

  belongs_to :user
  belongs_to :story


  private
  def reindex_story
    story.reindex
  end
end
