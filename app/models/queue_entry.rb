class QueueEntry < ApplicationRecord
  paginates_per 25
  scope :active, -> { where(deleted_at: nil).order('updated_at asc') }

  belongs_to :story, -> { includes :author }
  belongs_to :user

  def soft_delete
    self.deleted_at = DateTime.now
    save!
  end
end
