class AddOriginToStoryClicks < ActiveRecord::Migration[6.0]
  def change
    add_column :story_clicks, :origin, :string
  end
end
