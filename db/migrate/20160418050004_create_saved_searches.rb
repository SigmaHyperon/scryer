class CreateSavedSearches < ActiveRecord::Migration[4.2]
  def change
    create_table :saved_searches do |t|
      t.references :user
      t.index :user_id

      t.text :name

      t.timestamps null: false
    end
  end
end
