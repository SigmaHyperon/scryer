class AddSavedSearchIdToSearch < ActiveRecord::Migration[4.2]
  def change
    add_column :searches, :saved_search_id, :integer
    add_index :searches, :saved_search_id
  end
end
