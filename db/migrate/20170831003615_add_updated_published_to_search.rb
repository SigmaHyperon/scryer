class AddUpdatedPublishedToSearch < ActiveRecord::Migration[4.2]
  def change
    add_column :searches, :updated_before, :datetime
    add_column :searches, :updated_after, :datetime
    add_column :searches, :published_before, :datetime
    add_column :searches, :published_after, :datetime
  end
end
