class CreateStoryClicks < ActiveRecord::Migration[4.2]
  def change
    create_table :story_clicks do |t|
      t.integer :story_id, null: false
      t.integer :story_update_id
      t.integer :user_id
      t.jsonb :search
      t.integer :page, null: false
      t.string :ip
      t.string :user_agent

      t.timestamps null: false

      t.index :story_id
      t.index :user_id
      t.index [:story_id, :user_id]
    end
  end
end
