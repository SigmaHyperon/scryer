class MoveTagsToIds < ActiveRecord::Migration[4.2]
  def change
    change_table :searches do |t|
      t.rename :tags_include, :tags_include_id
      t.rename :tags_exclude, :tags_exclude_id
    end
  end
end
