class ChangeEventRawToJsonb < ActiveRecord::Migration[4.2]
  def up
    change_column :pensieve_events, :raw_event, 'jsonb USING CAST(raw_event AS jsonb)'
  end
end
