class AddTagsToStory < ActiveRecord::Migration[6.0]
  def change
    add_column :stories, :tags, :string, null: false, :array => true, default: []
  end
end
