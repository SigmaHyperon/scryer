class AddSearchTable < ActiveRecord::Migration[4.2]
  def change
    create_table :searches do |t|
      t.integer :fandoms, array: true
      t.integer :crossovers, array: true
      t.boolean :include_source_fandoms, default: false

      t.text :title
      t.text :author
      t.text :summary

      t.integer :category_required, array: true
      t.integer :category_optional, array: true
      t.boolean :category_optional_exclude, default: false

      t.integer :character_required, array: true
      t.integer :character_optional, array: true
      t.boolean :character_optional_exclude, default: false

      t.text :language
      t.text :status
      t.text :rating, array: true

      t.integer :wordcount_lower
      t.integer :wordcount_upper
      t.integer :chapters_lower
      t.integer :chapters_upper

      t.text :sort_by
      t.text :order_by

      t.timestamps null: false
    end
  end
end
