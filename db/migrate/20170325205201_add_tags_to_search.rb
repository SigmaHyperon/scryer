class AddTagsToSearch < ActiveRecord::Migration[4.2]
  def change
    change_table :searches do |t|
      t.integer :tags_include, null: false, :array => true, default: []
      t.integer :tags_exclude, null: false, :array => true, default: []
    end
  end
end
