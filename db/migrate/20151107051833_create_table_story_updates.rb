class CreateTableStoryUpdates < ActiveRecord::Migration[4.2]
  def change
    create_table :story_updates do |t|
      t.bigint :story_id, null: false
      t.text :story_title, null: false
      t.bigint :author_id, null: false
      t.text :author_name, null: false
      t.integer :fandoms, array: true, default: [], null: false
      t.jsonb :update_contents, null: false
      t.datetime :crawled_at, null: false
      t.datetime :updated_at, null: false

      t.index :story_id
      t.index :updated_at, order: {updated_at: :desc}
    end
  end
end
