class AddRelationshipsToSearch < ActiveRecord::Migration[5.1]
  def change
    change_table :searches do |t|
      t.jsonb :relationships
    end
  end
end
