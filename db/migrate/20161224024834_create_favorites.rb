class CreateFavorites < ActiveRecord::Migration[4.2]
  def change
    create_table :favorites do |t|
      t.references :story, index: true, foreign_key: false
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false

      t.index [:story_id, :user_id], unique: true
    end
  end
end
