class AddExcludeCrossoversToSearch < ActiveRecord::Migration[6.0]
  def change
    add_column :searches, :exclude_crossover_fandoms, :boolean, default: false
  end
end
