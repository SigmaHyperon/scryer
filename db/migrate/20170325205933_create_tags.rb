class CreateTags < ActiveRecord::Migration[4.2]
  def change
    create_table :tags do |t|
      t.string :name, limit: 20, null: false
      t.boolean :system, null: false, default: false

      t.timestamps null: false

      t.index :name, unique: true
    end
  end
end
