require 'test_helper'
require 'hashie/mash'

class SearchHelperTest < ActionView::TestCase
  include SearchHelper

  test 'classify_multi_1' do
    story = [Hashie::Mash.new({
        :summary => 'Harry blah blah Harry / Multi',
        :meta => {:relationships => []}
    }),
    Hashie::Mash.new({
        :summary => 'Harry blah blah Harry xMulti',
        :meta => {:relationships => []}
    }),
    Hashie::Mash.new({
        :summary => 'Harry blah blah',
        :meta => {:relationships => [['Harry', 'Person1', 'Person2']]}
    })]

    story.each { |s |
      assert_dom_equal('<span class="label label-warning">multi</span>', classify(s))
    }
  end

  test 'classify_harem_1' do
    story = [Hashie::Mash.new({
        :summary => 'Harry blah blah Harry / Harem',
        :meta => {:relationships => []}
    }),
    Hashie::Mash.new({
        :summary => 'Harry blah blah Harry/Person1/Person2/Person3',
        :meta => {:relationships => [['Harry', 'Person1', 'Person2']]}
    }),
    Hashie::Mash.new({
        :summary => 'Harry blah blah',
        :meta => {:relationships => [['Harry', 'Person1', 'Person2', 'Person3']]}
    })]

    story.each { |s |
      assert_dom_equal('<span class="label label-warning">harem</span>', classify(s))
    }
  end

  test 'classify_gender_bend' do
    story = [Hashie::Mash.new({
        :summary => 'Fem!Harry',
        :meta => {:relationships => []}
    }),
    Hashie::Mash.new({
        :summary => 'Fem!Naruto blah blah Harry/Person1/Person2/Person3',
        :meta => {:relationships => []}
    }),
    Hashie::Mash.new({
        :summary => 'FemHarry blah blah Harry/Person1/Person2/Person3',
        :meta => {:relationships => []}
    })]

    story.each { |s |
      assert_dom_equal('<span class="label label-warning">gender-bender</span>', classify(s))
    }
  end

  test 'classify_slash_1' do
    story = [Hashie::Mash.new({
        :summary => 'Harry blah blah slash',
        :meta => {:relationships => []}
    }),
    Hashie::Mash.new({
        :summary => 'Harry blah blah mpreg',
        :meta => {:relationships => []}
    }),
    Hashie::Mash.new({
        :summary => 'Harry blah blah M/M',
        :meta => {:relationships => []}
    })]
    story_not = [Hashie::Mash.new({
        :summary => 'Harry blah blah not slash',
        :meta => {:relationships => []}
    }),
    Hashie::Mash.new({
        :summary => 'Harry blah blah no slash',
        :meta => {:relationships => []}
    }),
    Hashie::Mash.new({
        :summary => 'Harry blah blah not a slash story',
        :meta => {:relationships => []}
    })]

    story.each { |s |
      assert_dom_equal('<span class="label label-danger">slash</span>', classify(s))
    }
    story_not.each { |s |
      assert_dom_equal('', classify(s))
    }
  end

  test 'votes_divided' do
    assert_in_delta(0.16666666666666666, vote(Hashie::Mash.new({:votetotal => 30, :votenum => 5})), 1e-6)
  end
end
